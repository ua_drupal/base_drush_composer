# Base PHP image with Composer and Drush

This is derived from the official Docker PHP image, using the relatively
recent 7.3 version (now largely Drupal 7 comatible). It enables additional PHP
extensions through the docker-php-ext-install script the official image
provides (with any additional system dependencies for these added in the usual
way), then adds Composer and Drush installations to the PHP installation.
